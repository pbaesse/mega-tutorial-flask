from flask import render_template, flash, redirect
from app import app
from app.forms import LoginForm

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Baesse'}

    posts = [
        {
            'author': {'username': 'Antonieta'},
            'body': 'Dia lindo em Natal!'
        },
        {
            'author': {'username': 'Sussana'},
            'body': 'Adoro o filme Matrix'
        }
    ]

    return render_template('index.html', title='Home', user=user, posts = posts)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login necessário para usuário {}, remember_me={}'.format(form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html', title="Entre!", form=form)


## Terminado web forms - parte 3
# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iii-web-forms